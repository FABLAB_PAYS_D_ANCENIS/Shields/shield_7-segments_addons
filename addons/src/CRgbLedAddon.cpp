#include "CRgbLedAddon.h"

/*****************************************************************************
 * Function	: CRgbLedAddon                                                   *
 *...........................................................................*
 * Description :                                                             *
 *****************************************************************************/
CRgbLedAddon::CRgbLedAddon()
{
	m_bInitDone = false;
}
	
/*****************************************************************************
 * Function	: init                                                           *
 *...........................................................................*
 * Description :                                                             *
 *****************************************************************************/
void CRgbLedAddon::init(uint16 u16NbLeds, uint8 u8Output)
{
	m_bInitDone = true;
	
	// Setup pin
	switch (u8Output)
	{
		case 1:  // A1
		    m_u8Output = cPinExtA1;			
		break;
		
		case 2:  // A2
			m_u8Output = cPinExtA2;			
		break;
		
		case 3:  // B1
			m_u8Output = cPinExtB1;			
		break;
		
		case 4:  // B2
			m_u8Output = cPinExtB2;				
		break;
				
		case 5:  // C1
			m_u8Output = cPinExtC1;			
		break;
		
		case 6:  // C2
			m_u8Output = cPinExtC2;	
		break;
		
		case 7:  // D1
			m_u8Output = cPinExtD1;	
		break;
		
		case 8:  // D2
			m_u8Output = cPinExtD2;	
		break;
		
		case 9:  // E1
			m_u8Output = cPinExtE1;	
		break;
		
		case 10:  // E2
			m_u8Output = cPinExtE2;	
		break;
		
		default:
			m_bInitDone = false;
		break;		
	}
	
	if(m_bInitDone == true)
	{
		// Store nb leds
		m_u16NbLeds = u16NbLeds;
		
		// When we setup the NeoPixel library, we tell it how many pixels, and which pin to use to send signals.
		pixels = new Adafruit_NeoPixel(u16NbLeds, m_u8Output, NEO_GRB + NEO_KHZ800); 
		pixels->begin();
		
		for(int i = 0 ; i < m_u16NbLeds ; i++)
		{
			pixels->setPixelColor(i, pixels->Color(0,0,0)); // Shutdown all leds.
			pixels->show(); // This sends the updated pixel color to the hardware.
		}
	}
}

/*****************************************************************************
 * Function    : switchOnLed                                                 *
 *...........................................................................*
 * Description :                                                             *
 *****************************************************************************/
void CRgbLedAddon::switchOnLed(uint16 u16Led, uint8 u8Color)
{
	/* List of colors availables
	color == 0 -> Red
	color == 1 -> Orange
	color == 2 -> Green
	color == 3 -> Yellow
	color == 4 -> Blue
	color == 5 -> Indigo
	color == 6 -> Purple
	color == "other" -> White
	*/

	if(m_bInitDone == true)
	{
		if(u16Led > 0)
		{
			u16Led--;
		}

		if (u8Color == 0)
		{
			pixels->setPixelColor(u16Led, pixels->Color(10,0,0)); // Setcolor to red.
		}
		else if (u8Color == 1)
		{
			pixels->setPixelColor(u16Led, pixels->Color(10,5,0)); // Setcolor to orange.
		}
		else if (u8Color == 2)
		{
			pixels->setPixelColor(u16Led, pixels->Color(0,10,0)); // Setcolor to green.
		}
		else if (u8Color == 3)
		{
			pixels->setPixelColor(u16Led, pixels->Color(10,10,0)); // Setcolor to yellow.
		}
		else if (u8Color == 4)
		{
			pixels->setPixelColor(u16Led, pixels->Color(0,0,10)); // Setcolor to blue.
		}
		else if (u8Color == 5)
		{
			pixels->setPixelColor(u16Led, pixels->Color(5,0,10)); // Setcolor to indigo.
		}
		else if (u8Color == 6)
		{
			pixels->setPixelColor(u16Led, pixels->Color(10,0,10)); // Setcolor to purple.
		}
		else
		{
			pixels->setPixelColor(u16Led, pixels->Color(10,10,10)); // Setcolor to white.
		}

		pixels->show(); // This sends the updated pixel color to the hardware.
	}
}

/*****************************************************************************
 * Function    : switchOffLed                                                *
 *...........................................................................*
 * Description :                                                             *
 *****************************************************************************/
void CRgbLedAddon::switchOffLed(uint16 u16Led)
{
	if(m_bInitDone == true)
	{
		if(u16Led > 0)
			u16Led--;

		pixels->setPixelColor(u16Led, pixels->Color(0,0,0)); // Shutdown led.
		pixels->show(); // This sends the updated pixel color to the hardware.
	}
}

/*****************************************************************************
 * Function    : switchOffAllLeds                                            *
 *...........................................................................*
 * Description :                                                             *
 *****************************************************************************/
void CRgbLedAddon::switchOffAllLeds(void)
{
	if(m_bInitDone == true)
	{
		for(int i = 0 ; i < m_u16NbLeds ; i++)
		{
			pixels->setPixelColor(i, pixels->Color(0,0,0)); // Shutdown all leds.
		}
		pixels->show(); // This sends the updated pixel color to the hardware.
	}
}

/*****************************************************************************
 * Function    : switchOnLedRgb                                              *
 *...........................................................................*
 * Description :                                                             *
 *****************************************************************************/
void CRgbLedAddon::switchOnLedRgb(uint16 u16Led, uint8 u8R, uint8 u8G, uint8 u8B)
{
	if(m_bInitDone == true)
	{
		if(u16Led > 0)
			u16Led--;

		pixels->setPixelColor(u16Led, pixels->Color(u8R,u8G,u8B)); // Setcolor to specific color.
		pixels->show();
	}
}
