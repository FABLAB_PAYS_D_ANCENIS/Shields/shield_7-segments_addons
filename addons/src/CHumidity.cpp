#include "CHumidity.h"

/*****************************************************************************
 * Function    : CButton                                                     *
 *...........................................................................*
 * Description :                                                             *
 *****************************************************************************/
CHumidity::CHumidity(uint8 u8Input)
{
	m_bInitDone = true;
	
    // Setup pin
	switch (u8Input)
	{
		case 1:  // A1
		    m_u8Input = cPinExtA1;
		break;
		
		case 2:  // A2
			m_u8Input = cPinExtA2;			
		break;
		
		case 3:  // B1
			m_u8Input = cPinExtB1;			
		break;
		
		case 4:  // B2
			m_u8Input = cPinExtB2;				
		break;
				
		case 5:  // C1
			m_u8Input = cPinExtC1;				
		break;
		
		case 6:  // C2
			m_u8Input = cPinExtC2;	
		break;
		
		case 7:  // D1
			m_u8Input = cPinExtD1;	
		break;
		
		case 8:  // D2
			m_u8Input = cPinExtD2;	
		break;
		
		case 9:  // E1
			m_u8Input = cPinExtE1;
		break;
		
		case 10:  // E2
			m_u8Input = cPinExtE2;
		break;
		
		default:
			m_bInitDone = false;
		break;		
	}
	
	if(m_bInitDone == true)
	{
		// Declaration
		hum = new DHT(m_u8Input, 11);
		
		// Start
		hum->begin();
	}
}

/*****************************************************************************
 * Function    : getValue                                                    *
 *...........................................................................*
 * Description :                                                             *
 *****************************************************************************/
int CHumidity::getValue(void)
{
	if(m_bInitDone == true)
	{
		// Humidity
		return (int)(hum->readHumidity());
	}
	else
	{
		return 0;
	}
}
