#ifndef COUTPUT_H
#define COUTPUT_H

#include "generic.h"

class COutput
{
public:
    COutput(uint8 u8Output, bool bLsMode);
    void setValue(bool bValue);

private:
	bool  m_bOutLsMode;
	uint8 m_u8Output;
	bool  m_bInitDone;
};

#endif // COUTPUT_H
