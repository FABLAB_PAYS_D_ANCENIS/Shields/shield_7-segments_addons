#ifndef INPUTDIN_H
#define INPUTDIN_H

#include "generic.h"

class CInputDin
{
public:
    CInputDin(uint8 u8Input, bool bLsMode, bool bPullUp);
    bool getValue(void);

private:
	uint8 m_u8Input;
	bool  m_bOutLsMode;
	bool  m_bInitDone;
};

#endif // INPUTDIN_H
