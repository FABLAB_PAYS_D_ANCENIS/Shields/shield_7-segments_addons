#ifndef CHUMIDITY_H
#define CHUMIDITY_H

#include "generic.h"
#include "DHT.h"

class CHumidity
{
public:
    CHumidity(uint8 u8Input);
    int getValue(void);

private:
	uint8 m_u8Input;
	bool  m_bInitDone;
	DHT *hum;
};

#endif // CHUMIDITY_H
