#ifndef INPUTANA_H
#define INPUTANA_H

#include "generic.h"

class CInputAna
{
public:
    CInputAna(uint8 u8Input);
    uint16 getValue(void);

private:
	uint8 m_u8Input;
	bool  m_bInitDone;
};

#endif // INPUTANA_H
