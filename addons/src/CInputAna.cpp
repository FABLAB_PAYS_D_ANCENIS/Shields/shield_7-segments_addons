#include "CInputAna.h"

/*****************************************************************************
 * Function    : CInputAna                                                   *
 *...........................................................................*
 * Description :                                                             *
 *****************************************************************************/
CInputAna::CInputAna(uint8 u8Input)
{
	m_bInitDone = true;
	
    // Setup pin
	switch (u8Input)
	{
		case 1:  // A1
		    m_u8Input = cPinExtA1;
			pinMode(cPinExtA1, INPUT);				
		break;
		
		case 2:  // A2
			m_u8Input = cPinExtA2;
			pinMode(cPinExtA2, INPUT);				
		break;
		
		case 3:  // B1
			m_u8Input = cPinExtB1;
			pinMode(cPinExtB1, INPUT);				
		break;
				
		case 5:  // C1
			m_u8Input = cPinExtC1;
			pinMode(cPinExtC1, INPUT);				
		break;
		
		case 7:  // D1
			m_u8Input = cPinExtD1;
			pinMode(cPinExtD1, INPUT);	
		break;
		
		case 9:  // E1
			m_u8Input = cPinExtE1;
			pinMode(cPinExtE1, INPUT);	
		break;
		
		default:
			m_bInitDone = false;
		break;		
	}
}

/*****************************************************************************
 * Function    : getValue                                                    *
 *...........................................................................*
 * Description :                                                             *
 *****************************************************************************/
uint16 CInputAna::getValue(void)
{
	if(m_bInitDone == true)
	{
		return analogRead(m_u8Input);
	}
	else
	{
		return 0;
	}
}