#include "CLuminosityAddon.h"

/*****************************************************************************
 * Function    : CLuminosityAddon                                           *
 *...........................................................................*
 * Description :                                                             *
 *****************************************************************************/
CLuminosityAddon::CLuminosityAddon(uint8 u8Input)
{
	m_bInitDone = true;
	
    // Setup pin
	switch (u8Input)
	{
		case 1:  // A1
		    m_u8Input = cPinExtA1;
		break;
		
		case 2:  // A2
			m_u8Input = cPinExtA2;			
		break;
		
		case 3:  // B1
			m_u8Input = cPinExtB1;			
		break;
				
		case 5:  // C1
			m_u8Input = cPinExtC1;				
		break;
		
		case 7:  // D1
			m_u8Input = cPinExtD1;	
		break;
		
		case 9:  // E1
			m_u8Input = cPinExtE1;
		break;
		
		default:
			m_bInitDone = false;
		break;		
	}
	
	if(m_bInitDone == true)
	{
		// Setup pin
		photocell = new LightDependentResistor (m_u8Input, 750, LightDependentResistor::GL5516);

		// Set photocell to ground
		photocell->setPhotocellPositionOnGround(true);
	}
}

/*****************************************************************************
 * Function    : getValue                                                    *
 *...........................................................................*
 * Description :                                                             *
 *****************************************************************************/
uint8 CLuminosityAddon::getValue()
{
    float intensity_in_lux;
    float intensity_in_pct;
    
	if(m_bInitDone == true)
	{
		// Get intensity inlux
		intensity_in_lux = photocell->getCurrentLux();

		// Convert it to %
		intensity_in_pct = 40*pow(intensity_in_lux,0.1);

		// Saturate to 100%
		if(intensity_in_pct > 100)
			intensity_in_pct = 100;

		// Saturate to 0%
		if(intensity_in_pct < 0)
			intensity_in_pct = 0;
			
		return (intensity_in_pct);
	}
	else
	{
		return 0;
	}
}
