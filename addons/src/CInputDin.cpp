#include "CInputDin.h"

/*****************************************************************************
 * Function    : CInputDin                                                   *
 *...........................................................................*
 * Description :                                                             *
 *****************************************************************************/
CInputDin::CInputDin(uint8 u8Input, bool bLsMode, bool bPullUp)
{
	m_bOutLsMode = bLsMode;	
	m_bInitDone = true;
	
    // Setup pin
	switch (u8Input)
	{
		case 1:  // A1
		    m_u8Input = cPinExtA1;
		break;
		
		case 2:  // A2
			m_u8Input = cPinExtA2;			
		break;
		
		case 3:  // B1
			m_u8Input = cPinExtB1;			
		break;
		
		case 4:  // B2
			m_u8Input = cPinExtB2;				
		break;
				
		case 5:  // C1
			m_u8Input = cPinExtC1;				
		break;
		
		case 6:  // C2
			m_u8Input = cPinExtC2;	
		break;
		
		case 7:  // D1
			m_u8Input = cPinExtD1;	
		break;
		
		case 8:  // D2
			m_u8Input = cPinExtD2;	
		break;
		
		case 9:  // E1
			m_u8Input = cPinExtE1;
		break;
		
		case 10:  // E2
			m_u8Input = cPinExtE2;
		break;
		
		default:
			m_bInitDone = false;
		break;		
	}
	
	if(m_bInitDone == true)
	{	
		if(bPullUp == false)
			pinMode(m_u8Input, INPUT);				
		else
			pinMode(m_u8Input, INPUT_PULLUP);				
	}
}

/*****************************************************************************
 * Function    : getValue                                                    *
 *...........................................................................*
 * Description :                                                             *
 *****************************************************************************/
bool CInputDin::getValue(void)
{
	if(m_bInitDone == true)
	{
		return digitalRead(m_u8Input^m_bOutLsMode);
	}
	else
	{
		return 0;
	}
}
