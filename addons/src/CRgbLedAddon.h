#ifndef CRGBLEDADDON_H
#define CRGBLEDADDON_H

#include "generic.h"
#include "Adafruit_NeoPixel.h"

class CRgbLedAddon
{
public:
    CRgbLedAddon();
	void init(uint16 u16NbLeds, uint8 u8Output);
    void switchOnLed(uint16 u16Led, uint8 u8Color);
    void switchOffLed(uint16 u16Led);
    void switchOffAllLeds(void);
    void switchOnLedRgb(uint16 u16Led, uint8 u8R, uint8 u8G, uint8 u8B);

private:
    Adafruit_NeoPixel *pixels;
	uint8  m_u8Output;
	uint16 m_u16NbLeds;
	bool  m_bInitDone;
};

#endif // CRGBLEDADDON_H
