#include "COutput.h"

/*****************************************************************************
 * Function    : COutput                                                     *
 *...........................................................................*
 * Description :                                                             *
 *****************************************************************************/
COutput::COutput(uint8 u8Output, bool bLsMode)
{
	m_bOutLsMode = bLsMode;	
	m_bInitDone = true;
	
    // Setup pin
	switch (u8Output)
	{
		case 1:  // A1
		    m_u8Output = cPinExtA1;			
		break;
		
		case 2:  // A2
			m_u8Output = cPinExtA2;			
		break;
		
		case 3:  // B1
			m_u8Output = cPinExtB1;			
		break;
		
		case 4:  // B2
			m_u8Output = cPinExtB2;				
		break;
				
		case 5:  // C1
			m_u8Output = cPinExtC1;			
		break;
		
		case 6:  // C2
			m_u8Output = cPinExtC2;	
		break;
		
		case 7:  // D1
			m_u8Output = cPinExtD1;	
		break;
		
		case 8:  // D2
			m_u8Output = cPinExtD2;	
		break;
		
		case 9:  // E1
			m_u8Output = cPinExtE1;	
		break;
		
		case 10:  // E2
			m_u8Output = cPinExtE2;	
		break;
		
		default:
			m_bInitDone = false;
		break;		
	}

	if(m_bInitDone == true)
	{	
		pinMode(m_u8Output, OUTPUT);
		setValue(false);
	}
}

/*****************************************************************************
 * Function    : setValue                                                    *
 *...........................................................................*
 * Description :                                                             *
 *****************************************************************************/
void COutput::setValue(bool bValue)
{
	if(m_bInitDone == true)
	{	
		digitalWrite(m_u8Output, bValue^m_bOutLsMode);
	}
}
