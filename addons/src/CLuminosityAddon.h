#ifndef CLUMINOSITYADDON_H
#define CLUMINOSITYADDON_H

#include "generic.h"
#include "LightDependentResistor.h"

class CLuminosityAddon
{
public:
    CLuminosityAddon(uint8 u8Input);
    uint8 getValue();
private:
	uint8 m_u8Input;
	bool  m_bInitDone;
    LightDependentResistor *photocell;

};

#endif // CLUMINOSITYADDON_H
